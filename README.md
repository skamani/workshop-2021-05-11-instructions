# Workshop 2021-05-11 Instructions

Instructions to follow along with Lab materials for the Workshop presented on May 11, 2021.

Please download and review the [Exercises document](https://gitlab.com/skamani/workshop-2021-05-11-instructions/-/blob/master/GitLab%20Security%20Workshop%20-%2005_11_2021%20-%20Take%20home%20exercises.pdf).

# Related Projects:
- [Project 1](https://gitlab.com/skamani/wshp-p1) used in exercises.
- [Project 2](https://gitlab.com/skamani/wshp-p2) used in exercises.

